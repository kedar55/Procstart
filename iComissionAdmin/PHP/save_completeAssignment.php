<?php

//SAVE current stage number of assignment

include("db_connection.php");

$data = json_decode(file_get_contents("php://input"));

$ProjectSeekerID = $data->UserID; // JobSeekerId
$ProjectID =  $data->ProjectID;
$UserID =  $data->ClientID; // UserID Who Create Assignemnt
$CompleteFlag =  $data->CompleteFlag;

/// CompleteFlag 1- Open 2- Complete by Seeker, 3 - Complete by Client 4- Dispute by Seeker 5- Dispute by Client


$sql0 = "CALL usp_Insert_ProjectCompleteAssignment(?,?,?,?)";
$stmt0 = $conn->prepare($sql0);

  $stmt0->bind_param("iiii",$UserID,$ProjectSeekerID, $ProjectID,$CompleteFlag);

if($stmt0->execute())
{                                  
	$data = true;
	$msg = 'data saved';
	$respone = array('success'=>$data);            
	echo json_encode($respone);
}
else
{
	$data = false;
	$msg ='error in saving data  of CompleteFlag';
	$respone = array('success'=>$data,'msg'=>$msg);
	echo json_encode($respone);
}

?>