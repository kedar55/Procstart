<?php
//search job by different filter like job location exp,salary

//SELECT * FROM `jobpost` WHERE `JobLocation` LIKE '%Pune%'
include("db_connection.php");

$data = json_decode(file_get_contents("php://input"));


    $joblocation = $data->joblocation;
    $Experience = $data->Experience;
    $Salary = $data->Salary;
    $keywords = $data->keywords;
    $sortby = $data->sortby;

    $type = $data->type;

    if($type != "No"){
        $type = "j1.MembershipType = '".$type."' and";
    }
    else{
        $type = "";
    }

    if($sortby != "No")
    {
        if($sortby == "SalaryLowtohigh"){
            $sortby = "order by j1.MinSal asc";
        }
        else if($sortby == "Salaryhightolow"){
            $sortby = "order by j1.MinSal desc";
        }
        else if($sortby == "Experiencelowtohigh"){
            $sortby = "order by j1.MinExp asc";
        }
        else if($sortby == "Experiencehightolow"){
            $sortby = "order by j1.MinExp desc";
        }
    }
    else{
        $sortby = "order by j1.JobPostDate DESC";
    }

    if($Experience != "No")
    {
        $Expfirst = reset($Experience);
        $Explast = end($Experience);

        $FinalExperience = "j1.MinExp >= ".$Expfirst." and j1.MaxExp <= ".$Explast." and";
    }
    else{
        $FinalExperience = "";
    }

    if($Salary != "No")
    {
        $Salfirst = reset($Salary);
        $Sallast = end($Salary);

        $FinalSalary = "j1.MinSal >= ".$Salfirst." and j1.MaxSal <= ".$Sallast." and";
    }
    else{
        $FinalSalary = "";
    }

    if($keywords == "No")
    {
        $keywords = "";
    }
    else{
        $keywords = "j2.SkillSetID LIKE '%".$keywords."%' and";
    }

    if($joblocation == "No")
    {
        $joblocation = "";
    }
    else{
        $joblocation = "j1.JobLocation LIKE '%".$joblocation."%' and";
    }
    
    
    // $keywordss = explode(",",$keywords);
    // $keywrds=[];
    // for($j=0; $j< sizeof($keywordss);$j++){
    //     $keywrds[$j] = "j2.SkillSetID LIKE '%".$keywordss[$j]."%'";
    // }
    // $Finalkeywords = implode(" or ",$keywrds);

    $sql1 = "SELECT j1.*,j2.* FROM jobpost j1 inner join jobpostskillset j2 WHERE $keywords $joblocation  $FinalExperience $FinalSalary $type j1.JobPostID = j2.JobPostID and j1.JobActive = 'Active' group by j1.JobPostID $sortby";
    //$sql1 = "SELECT j1.*,j2.* FROM jobpost j1 inner join jobpostskillset j2 WHERE (j2.SkillSetID LIKE '%$keywords%' or j2.SkillSetID ='') and (j1.JobLocation LIKE '%$joblocation%' or j1.JobLocation = '') and (j1.MinExp = '$Experience' or j1.MinExp='') and (j1.MinSal = '$Salary' or j1.MinSal='') and j1.JobPostID = j2.JobPostID order by j1.JobPostDate DESC";
    //echo $sql1;
            $result1 = $conn->query($sql1);
            
            if ($result1->num_rows > 0) 
            {
       
                while($row = $result1->fetch_assoc())
                {
                    $output = $row;
    
                    $id = $row['JobPostID'];
                    $JobPostedBy = $row['JobPostedBy'];

                    $sql3 ="SELECT Lat,Lng,CompanyName,CompanyURL,CompanyLogo FROM registeruserinforamtion where UserID = $JobPostedBy";
                    $result3 = $conn->query($sql3);
                    if ($result3->num_rows > 0)
                    {
                        while($row3 = $result3->fetch_assoc())
                        {  
                            $Lat = $row3['Lat'];
                            $Lng = $row3['Lng']; 
                            $CompanyName = $row3['CompanyName'];
                            $CompanyURL = $row3['CompanyURL'];
                            $CompanyLogo = $row3['CompanyLogo'];
                        }
                    }

                    // $sql5 ="SELECT COUNT(JobPostID) As NumberOFjobs FROM jobpost where JobPostedBy = $JobPostedBy";
                    // $result5= $conn->query($sql5);
                    // if ($result5->num_rows > 0) 
                    // {
                    //     while($row5 = $result5->fetch_assoc())
                    //     {
                    //         $NumberOFjobs = $row5['NumberOFjobs'];
                    //     }
                    // }
                    
                    $sql2 ="SELECT COUNT(JobApplyActivityID) As NumberOFCandidate FROM jobapplyactivity where JobPostID = $id ";
                    
                    $result2 = $conn->query($sql2);

                    if ($result2->num_rows > 0) 
                    {
                        while($row2 = $result2->fetch_assoc())
                        {
                            $noofcandidate = $row2['NumberOFCandidate'];
                        }
                    }
                    else
                    {
                        $data = false;
                        $msg ='No data';
                        $respone = array('data'=>$data,'msg'=>$msg);
                        echo json_encode($respone);
                    }
                    $output['NumberOFCandidate_Applied'] =  $noofcandidate; 
                    $output['Lat'] =  $Lat; 
                    $output['Lng'] =  $Lng;
                    $output['CompanyName'] =  $CompanyName; 
                    $output['CompanyURL'] =  $CompanyURL;
                    $output['CompanyLogo'] =  $CompanyLogo; 
                    $output['NumberOFjobs'] =  "0";

                    $respone[]= $output; 
                                                                                                             
                }
                echo json_encode($respone);
    
            }
            else
            {
                // $data = false;
                // $msg ='No data';
                // $respone = array('data'=>$data,'msg'=>$msg);
                echo "error";
            }
?>