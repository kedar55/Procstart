<?php

//SAVE current stage number of assignment

include("db_connection.php");

$data = json_decode(file_get_contents("php://input"));

$ProjectSeekerID = $data->UserID; // JobSeekerId
$ProjectID =  $data->ProjectID;
$StageNo =  $data->StageNo;

$sql = "SELECT ProjectAssignmentStepID FROM projectassignmentstep where ProjectPostID = $ProjectID and JobSeekerId = $ProjectSeekerID";// and UserID = $UserID";

$result= $conn->query($sql);
if ($result->num_rows>0) 
{
	while($row = $result->fetch_assoc())
    {   
        $output = $row;
    }

	$in_ProjectAssignmentStepID = $output['ProjectAssignmentStageSeekerID'];
	
	$sql0 = "CALL usp_Update_ProjectAssignmentStageSeeker(?,?,?,?)";
    $stmt0 = $conn->prepare($sql0);
    
    $stmt0->bind_param("iiii",$in_ProjectAssignmentStepID,$ProjectID,$ProjectSeekerID,$StageNo);
    
    if($stmt0->execute())
    {                                  
        $data = true;
        //$msg = 'data saved';
        $respone = array('success'=>$data);            
        echo json_encode($respone);
    }
    else
    {
        $data = false;
        $msg ='error in update data of projectassignmentstep';
        $respone = array('success'=>$data,'msg'=>$msg);
        echo json_encode($respone);
    }
	
  
}
else
{
    $sql0 = "CALL usp_Insert_ProjectAssignmentStageSeeker(?,?,?)";
    $stmt0 = $conn->prepare($sql0);
    
      $stmt0->bind_param("iiii",$ProjectID,$ProjectSeekerID,$StageNo);
  
    if($stmt0->execute())
    {                                  
        $data = true;
        $msg = 'data saved';
        $respone = array('success'=>$data);            
        echo json_encode($respone);
    }
    else
    {
        $data = false;
        $msg ='error in saving data  of projectassignmentstep';
        $respone = array('success'=>$data,'msg'=>$msg);
        echo json_encode($respone);
    }
}
?>