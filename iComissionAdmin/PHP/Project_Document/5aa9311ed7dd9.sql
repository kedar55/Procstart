DELIMITER $$


DROP PROCEDURE IF EXISTS usp_Insert_ProjectDcoumentUpload$$
CREATE PROCEDURE `usp_Insert_ProjectDcoumentUpload`(
IN `In_ProjectUserID` INT, IN `In_ProjectPostID` INT, 
IN `In_ProjectSeekerID` INT, 
IN `In_FilePath` VARCHAR(500),
IN `In_FileName` VARCHAR(200)
)
    NO SQL
BEGIN
       INSERT INTO `projectcontractdocumentupload`(
       `UserID`, `JobSeekerID`, `ProjectPostID`, `filePath`,`fileName`) 
        VALUES (`In_ProjectUserID`, `In_ProjectSeekerID`,`In_ProjectPostID`, 
        `In_FilePath`,`In_FileName`);
       
END$$



DROP PROCEDURE IF EXISTS usp_Update_ProjectDcoumentUpload$$
CREATE PROCEDURE `usp_Update_ProjectDcoumentUpload`(
IN `in_ProjectContractDocumentID` INT, 
IN `In_ProjectUserID` INT, 
IN `In_ProjectPostID` INT, 
IN `In_ProjectSeekerID` INT, 
IN `In_FilePath` VARCHAR(500),
IN `In_FileName` VARCHAR(200)
)
    NO SQL
BEGIN
       update `projectcontractdocumentupload`      
       set `filePath` = `In_FilePath`,
       `fileName` = `In_FileName`       
       where 
       `UserID` = `In_ProjectUserID`
       and `JobSeekerID` = `In_ProjectSeekerID`
       and `ProjectPostID` = `In_ProjectPostID`
       and `ProjectContractDocumentID` = `in_ProjectContractDocumentID`;
       
END$$

DELIMITER ;



DROP TABLE IF EXISTS `projectcontractdocumentupload`;
CREATE TABLE IF NOT EXISTS `projectcontractdocumentupload` (
  `ProjectContractDocumentID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  `JobSeekerID` int(11) NOT NULL,
  `ProjectPostID` int(11) NOT NULL,
  `filePath` varchar(500) NOT NULL DEFAULT '',
  `fileName` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`ProjectContractDocumentID`),
  KEY `UserID_idx` (`UserID`),
  KEY `JobSeekerID_idx` (`JobSeekerID`),
  KEY `ProjectPostID_idx` (`ProjectPostID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

